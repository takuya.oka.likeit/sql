﻿
CREATE TABLE item_category(
        category_id INT NOT NULL   primary key AUTO_INCREMENT,
        category_name VARCHAR(256) NOT NULL
        );
        
CREATE TABLE item(
        item_id INT NOT NULL primary key AUTO_INCREMENT,
        item_name VARCHAR(256) NOT NULL,
        item_price INT NOT NULL,
        category_id INT
        );


