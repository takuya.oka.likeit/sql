﻿
CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

use usermanagement;

CREATE TABLE user(
id SERIAL primary key AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL UNIQUE,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL);


INSERT INTO 
user
VALUES(
1,'admin','管理者','1956-03-01','aaa','2018-11-24 16:45:08','2018-12:18 17:58:25');

